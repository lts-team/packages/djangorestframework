Source: djangorestframework
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Brian May <bam@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 mkdocs,
 node-jquery,
 python3-all,
 python3-coreapi,
 python3-defusedxml,
 python3-django,
 python3-django-filters,
 python3-django-guardian (>= 2.4.0),
 python3-pygments,
 python3-pytest,
 python3-pytest-cov,
 python3-pytest-django (>= 3.5.1),
 python3-setuptools,
 python3-tz,
 python3-uritemplate,
 python3-yaml,
Standards-Version: 4.6.1.0
Homepage: https://www.django-rest-framework.org
Vcs-Browser: https://salsa.debian.org/python-team/packages/djangorestframework
Vcs-Git: https://salsa.debian.org/python-team/packages/djangorestframework.git
Rules-Requires-Root: no

Package: python-djangorestframework-doc
Section: doc
Architecture: all
Depends:
 fonts-glyphicons-halflings,
 ${misc:Depends},
 ${mkdocs:Depends},
Description: Web APIs for Django, made easy (documentation)
 powerful and flexible toolkit that makes it easy to build Web APIs.
 Some reasons you might want to use REST framework:
  * The Web browseable API is a huge useability win for your developers.
  * Authentication policies including OAuth1a and OAuth2 out of the box.
  * Serialization that supports both ORM and non-ORM data sources.
  * Customizable all the way down - just use regular function-based views if you
    don't need the more powerful features.
 .
 This package contains the HTML documentation.

Package: python3-djangorestframework
Architecture: all
Depends:
 fonts-font-awesome,
 fonts-glyphicons-halflings,
 libjs-bootstrap,
 libjs-jquery,
 libjs-prettify,
 python3-django,
 ${misc:Depends},
 ${python3:Depends},
Recommends:
 python3-coreapi,
 python3-coreschema,
 python3-django-filters,
 python3-django-guardian (>= 2.4.0),
 python3-markdown,
 python3-psycopg2,
 python3-yaml,
Suggests:
 python-djangorestframework-doc,
Description: Web APIs for Django, made easy for Python3
 For Python3.
 powerful and flexible toolkit that makes it easy to build Web APIs.
 Some reasons you might want to use REST framework:
  * The Web browseable API is a huge useability win for your developers.
  * Authentication policies including OAuth1a and OAuth2 out of the box.
  * Serialization that supports both ORM and non-ORM data sources.
  * Customizable all the way down - just use regular function-based views if you
    don't need the more powerful features.
  * Extensive documentation, and great community support.
